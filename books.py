import csv

#Block repeats on wrong input
while True:
    print("1. Title","2.Genere", sep = "\n")

    #Getting search criteria
    choice = int(input("What do you want to serch by (1 or 2):"))

    #Validating input
    if choice == 1 or choice == 2:

        #Search Keyword
        search = input("Search for :")
        break

#Reading CSV file
with open("nyt_bestsellers.csv", "r") as input:
    reader = csv.DictReader(input)

    count = 0 #Counter to track filtered data

    for row in reader:

        #Checking for matches by Title
        if choice == 1:

            if search.lower() == row["book_name"].lower():
                count += 1
                print(count, row["book_name"], row["genre"], sep = " | ")

        #Checking for Matches by Genre
        elif choice == 2:

                if search.lower() == row["genre"].lower():
                    count += 1
                    print(count, row["book_name"], row["genre"], sep = " | ")


    #If no data Matches
    if count == 0:
        print("Sorry, No Match Availabe...!")
